var wikidata_distributed_game = {

	thumbsize : 120 ,
	precache_games : 5 ,

	fallback_lang : 'en' ,
	running : 0 ,
	games : {} ,
	serial : 0 ,
	vertical : false ,
	widar : {} ,
	wd : {} ,
	autodesc_cache : {} ,
	shortcuts : {} ,
	user_settings : { languages:['en'] } ,

	init : function () {
		var me = this ;
		me.lang = me.fallback_lang ;
		
		me.wd = new WikiData ;

		$('#stats_icon a').click ( me.showStats ) ;
		
		me.vertical = (window.innerHeight > window.innerWidth) ;
		$('a.navbar-brand').click ( function () {
			me.showGames() ;
			return false ;
		} ) ;
		
		$(window).keypress ( me.onKeyPress ) ;


		function fin () {
			if ( me.isRunning(-1) ) return ;

			var h = '' ;
			if ( me.widar.isLoggedIn() ) {
				h = me.widar.getUserName() ;
				$('#user_icon').show() ;
				$('#user_icon a').click ( me.showUserSettings ) ;
			} else {
				h = me.widar.getLoginLink('Log in') ;
			}
			$('#widar').html(h).show() ;

			var v = me.getUrlVars() ;
			if ( typeof v.mode == 'undefined' && typeof v.game == 'undefined' ) me.showGames() ; // No need to wait for user data to load

			function fin2 () {
				if ( typeof v.mode == 'undefined' && typeof v.game == 'undefined' ) return ; // Had that
				
				if ( v.mode == 'settings' ) me.showUserSettings() ;
				else if ( v.mode == 'add_game' ) me.addNewGame() ;
				else if ( v.mode == 'stats' ) me.showStats() ;
				else if ( v.mode == 'test_game' ) me.testGame(decodeURIComponent(v.url)) ;
				else if ( typeof v.game != 'undefined' ) {
					var opt ;
					if ( typeof v.opt != 'undefined' ) opt = JSON.parse ( decodeURIComponent ( v.opt ) ) ;
					me.startGame ( v.game*1 , opt ) ;
				}
				else me.showGames() ;
			}
			
			
			if ( me.widar.isLoggedIn() ) {
				$.get ( './api.php' , {
					action:'get_user_settings',
					user:me.widar.getUserName()
				} , function ( d ) {
					me.user_settings = d.user_settings ;
				} ) . always ( fin2 ) ;
			} else {
				me.user_settings = { languages:['en'] } ;
				fin2() ;
			}
		}

		me.isRunning(1);
		me.widar = new WiDaR ( fin ) ;
		me.loadGames ( fin ) ;
	} ,
	
	onKeyPress : function ( e ) {
		if ( typeof e.key == 'undefined' ) e.key = String.fromCharCode ( e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0 ) ;
		var me = wikidata_distributed_game ;
		if ( typeof me.current_game == 'undefined' ) return ;
		if ( typeof me.current_game.cache == 'undefined' ) return ;
		if ( me.current_game.cache.length == 0 ) return ;
		var game_id = me.current_game.id ;
		var tile_id = me.current_game.cache[0].id ;
		if ( typeof me.shortcuts[game_id] == 'undefined' ) return ;
		if ( typeof me.shortcuts[game_id][tile_id] == 'undefined' ) return ;
		if ( typeof me.shortcuts[game_id][tile_id][e.key] == 'undefined' ) return ;
//		console.log ( game_id , tile_id , e.key , me.shortcuts[game_id][tile_id][e.key] ) ;
		$('#'+me.shortcuts[game_id][tile_id][e.key]).click() ;
	} ,
	
	isRunning : function ( diff ) {
		var me = this ;
		if ( typeof diff != 'undefined' ) {
			me.running += diff ;
			if ( me.running > 0 ) $('#spinning').show() ;
			else $('#spinning').hide() ;
		}
		return me.running > 0 ;
	} ,

	getUrlVars : function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURI(hash[1]);
		} ) ;
//		console.log ( vars ) ;
		return vars;
	} ,

	escapeAttribute : function ( s ) {
		if ( typeof s == 'undefined' ) s = '' ;
		return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
	} ,
	
	getMainLang : function () {
		var me = this ;
		if ( typeof me.user_settings != 'undefined' ) return me.user_settings.languages[0] ;
		return me.lang ;
	} ,
	
	t : function ( o ) {
		var me = this ;
		if ( typeof o == 'undefined' ) return 'No object passed' ;
		if ( typeof o[me.getMainLang()] != 'undefined' ) return o[me.getMainLang()] ;
		if ( typeof o[me.fallback_lang] != 'undefined' ) return o[me.fallback_lang] ;
		return "<i>No suitable translation found</i>" ;
	} ,
	
	toText : function ( s ) {
		var tmp = document.createElement("DIV");
		tmp.innerHTML = s;
		var ret = tmp.textContent || tmp.innerText || "";
		ret = ret.replace ( /\n/g , '<br/>' ) ;
		return ret ;
	} ,
	
	prepGames : function () {
		var me = this ;
		$.each ( me.games , function ( k , v ) {
			me.games[k].cache = [] ;
		} ) ;
	} ,
	
	loadGames : function ( callback ) {
		var me = this ;
		me.isRunning(1);
		$.get ( './games.json' , function ( d ) {
			me.games = d ;
			me.prepGames() ;
			callback() ;
		} , 'json' ) ;
	} ,
	
	renderGameEntry : function ( g ) {
		var me = this ;
		var h = '' ;
		h += "<div class='row game_entry' gameid='"+g.id+"'>" ;
		h += "<div class='col-xs-4 col-md-2'>" ;
		h += "<div class='wraptocenter' style='text-align:center;height:"+me.thumbsize+"px'><span></span>" ;
		if ( typeof g.icon != 'undefined' ) h += "<img class='img-responsive' src='" + me.escapeAttribute(g.icon) + "' />" ;
		h += "</div>" ;
		h += "</div>" ;
		h += "<div class='col-xs-8 col-md-10'>" ;
		h += "<h3>" + me.toText ( me.t ( g.label ) ) + "</h3>" ;
		h += "<div>" + me.toText ( me.t ( g.description ) ) + "</div>" ;
		h += "</div>" ;
		h += "</div>" ;
		return h ;
	} ,
	
	renderMessageBar : function ( msg ) {
		if ( typeof msg == 'undefined' ) return '' ;
		var me = this ;
		var h = '' ;
		h += '<div class="alert alert-'+msg['class']+' alert-dismissible" role="alert">' ;
		h += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' ;
		h += msg.msg ;
		h += '</div>' ;
		return h ;
	} ,
	
	showGames : function ( msg ) {
		var me = this ;
		window.location.hash = '' ;
		var h = '' ;
		h += me.renderMessageBar ( msg ) ;
		h += '<div class="panel panel-default">' ;
		h += '<div class="panel-heading">Available games</div>' ;
		h += '<div class="panel-body"><p>This is a list of available games. Games are loaded from different sources, and you can <a href="#" id="add_new_game">add your own!</a></p></div>' ;
		h += ' <ul class="list-group" id="game_list"></ul>' ;
		h += '</div>' ;
		$('#main').html(h) ;
		$('#add_new_game').click ( function () { me.addNewGame() ; return false } ) ;
		
		var keys = [] ;
		$.each ( me.games , function ( k , v ) { keys.push ( k*1 ) } ) ;
		keys = keys.sort ( function (a,b) { return b-a } ) ;
		
		$.each ( keys , function ( dummy , k ) {
			var v = me.games[k] ;
			$('#game_list').append ( '<li class="list-group-item">' + me.renderGameEntry ( v ) + '</li>' ) ;
		} ) ;
		$('div.game_entry').css({cursor:'pointer'}).click ( function () { me.startGame ( $(this).attr('gameid')*1 ) ; return false } ) ;
	} ,
	
	sanitizeQ : function ( q ) {
		return 'Q' + (''+q).replace(/\D/g,'') ;
	} ,
	
	controlClick : function ( event ) {
		var me = this ;

		if ( event.o.decision == 'skip' ) {
			return me.nextTile() ;
		}
		
		var g = me.current_game ;
		var name_of_the_game = me.toText ( me.t ( g.label ) ) ;
		
		var actions = event.o.api_action||[] ;
		if ( typeof actions.length == 'undefined' ) actions = [ actions ] ; // Single action => array
		var api_actions_j = JSON.stringify ( actions ) ; // For later
		
		function fin () {
			if ( me.isRunning(-1) ) return ;
		}
		
		// Log action in game central
		me.isRunning(1);
		$.get ( './api.php' , {
			action:'log_action',
			user:me.widar.getUserName(),
			game:g.id,
			tile:event.tile.id,
			decision:event.o.decision,
			api_action:api_actions_j
		} , function ( d ) {
//			console.log ( "GAME CENTRAL" , d ) ;
		} , 'json' ) . always ( function () { fin() } ) ;
		
		// Feedback to game
		me.isRunning(1);
		$.getJSON ( me.getBaseAPI(g) , {
			action:'log_action',
			user:me.widar.getUserName(),
			tile:event.tile.id,
			decision:event.o.decision
		} , function ( d ) {
//			console.log ( "DISTRIBUTED GAME" , d ) ;
		} ) . always ( function () { fin() } ) ;
		
		// Do Wikidata editing
		function do_action ( response ) {
			if ( typeof response != 'undefined' && response.error != 'OK' ) {
				console.log ( "API ERROR" , response ) ;
			}
			if ( actions.length == 0 ) return fin() ;
			var action = actions[0] ;
			actions.shift() ;
			action.summary = "The Distributed Game #" + g.id + ": " + name_of_the_game ;
			me.widar.genericAction ( action , do_action ) ;
		}
		me.isRunning(1);
		do_action() ;

		me.nextTile() ;
	} ,
	
	getBaseAPI : function ( g ) {
		if ( g.api.match(/\?/) ) return g.api+'&callback=?' ;
		return g.api+'?callback=?' ;
	} ,
	
	getQlink : function ( q , title ) {
		var me = this ;
		var qq = 'Q' + (''+q).replace(/\D/g,'') ;
		if ( typeof title == 'undefined' ) title = qq ;
		return "<a target='_blank' class='wikidata' href='//www.wikidata.org/wiki/" + qq + "'>" + me.toText(title) + "</a>" ;
	} ,
	
	renderMap : function ( o ) {
		var style = 'osm-intl';
		var server = 'https://maps.wikimedia.org/';
		
		// Create a map
		var map = L.map(o.id).setView([o.lat*1,o.lon*1], o.zoom*1);

		// Add a map layer
		L.tileLayer(server + style + '/{z}/{x}/{y}.png', {
			maxZoom: 18,
			id: o.id+'-01',
			attribution: 'Wikimedia maps beta | Map data &copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
		}).addTo(map);
		
		L.marker([o.lat*1, o.lon*1]).addTo(map);
		
//		setTimeout ( function () { map.invalidateSize() } , 10 ) ;
	} ,
	
	appendGame : function ( tile ) {
		var me = this ;
		var h = '' ;
		
		var maps = [] ;
		
		h += "<div class='gametile row' id='gametile_"+tile.secnum+"'>" ;
		
		// Sections
		$.each ( tile.sections , function ( section_number , sec ) {
			h += "<div class='col-md-12 section'>" ;
			if ( sec.type == 'item' ) {
				var q = me.sanitizeQ ( sec.q ) ;
				h += "<div class='item_preview' q='" + q + "'>" ;
				h += "<div class='item_label'>" + me.getQlink(q) + "...</div>" ;
				h += "<div class='item_options'></div>" ;
				h += "<div class='item_description clearfix'></div>" ;
				h += "</div>" ;
			} else if ( sec.type == 'text' ) {
				if ( typeof sec.title != 'undefined' ) {
					h += "<div class='sec_text_title'>" ;
					if ( typeof sec.url != 'undefined' ) h += "<a target='_blank' class='external' href='" + me.escapeAttribute(sec.url) + "'>" + me.toText(sec.title) + "</a>" ;
					else h += me.toText(sec.title) ;
					h += "</div>" ;
				}
				h += "<div class='sec_text'>" + me.toText ( sec.text||'' ) + "</div>" ;
			} else if ( sec.type == 'wikipage' ) {
				var m = sec.wiki.match ( /^(.+)wiki$/ ) ;
				if ( m == null ) console.log ( sec.wiki ) ;
				var l = m[1] ;
				var url = 'https://' + l + '.wikipedia.org/wiki/' + encodeURIComponent(sec.title.replace(/ /g,'_')) ;
				h += "<div class='sec_text_title'>" ;
				h += "<a target='_blank' class='external' href='" + me.escapeAttribute(url) + "'>" + me.toText(sec.title) + "</a>" ;
				h += " <small>[" + me.toText(sec.wiki) + "]</small>" ;
				h += "</div>" ;
				h += "<div class='wikipage_text' lang='"+me.escapeAttribute(l)+"' title='"+me.escapeAttribute(sec.title)+"'><i>Loading...</i></div>" ;
			} else if ( sec.type == 'map' ) {
				var id = 'map_' + me.serial ;
				me.serial++ ;
				h += "<div id='" + id + "' style='height:300px'></div>" ;
				maps.push ( { id:id, lat:sec.lat, lon:sec.lon, zoom:sec.zoom||6 } ) ;
			} else {
				console.log ( "Unknown section type" , sec ) ;
			}
			h += "</div>" ;
		} ) ;

		// Controls
		var control_actions = [] ;
		var shortcuts = { yes:1 , skip:2 , no:3 } ;
		var type2class = { green:'btn-success' , white:'btn-default' , blue:'btn-primary' , yellow:'btn-warning' } ;
		h += "<div class='col-md-12 control_box'><div class='row'>" ;
		if ( me.widar.isLoggedIn() ) {
			$.each ( tile.controls , function ( section_number , sec ) {
				h += "<div class='col-md-12 control'>" ;
				if ( sec.type == 'buttons' ) {
					h += "<div style='text-align:center'>" ;
					h += "<div class='btn-group' role='group' aria-label='...'>" ;
					$.each ( (sec.entries||[]) , function ( k , v ) {
						var control_id = 'control_' + me.serial ;
						me.serial++ ;
						h += "<button id='" + control_id + "' type='button' class='btn btn-lg" ;
						if ( typeof type2class[v.type||''] != 'undefined' ) h += ' ' + type2class[v.type] ;
						h += "'" ;
						
						var ak = '' ;
						if ( typeof shortcuts[v.decision] != 'undefined' ) ak = shortcuts[v.decision] ;
						else if ( typeof v.shortcut != 'undefined' && v.shortcut.length == 1 ) ak = me.toText(v.shortcut) ;
						
						if ( ak != '' ) {
							h += " title='Or press " + ak + "'" ;
							var game_id = me.current_game.id ;
							var tile_id = tile.id ;
							if ( typeof me.shortcuts[game_id] == 'undefined' ) me.shortcuts[game_id] = {} ;
							if ( typeof me.shortcuts[game_id][tile_id] == 'undefined' ) me.shortcuts[game_id][tile_id] = {} ;
							me.shortcuts[game_id][tile_id][ak] = control_id ;
						}
						
						h += ">" + v.label + "</button>" ;
						control_actions.push ( { id:control_id , tile:tile , o:v } ) ;
					} ) ;
					h += "</div>" ;
					h += "</div>" ;
				}
				h += "</div>" ;
			} ) ;
		} else {
			h += "<div class='col-md-12 control' style='text-align:center;font-size:14pt'>" ;
			h += "You need to " + me.widar.getLoginLink('log in') + " to play this game!" ;
			h += "</div>" ;
		}
		h += "</div></div>" ;

		h += "</div>" ;
		
		$('#tiles').append ( h ) ;


		
		// Post-coital cleanup
		
		// Button click events
		$.each ( control_actions , function ( k , v ) {
			$('#'+v.id).click ( function () {
				me.controlClick ( v ) ;
			} ) ;
		} ) ;
		
		// Wikipage
		$('#gametile_'+tile.secnum+' div.wikipage_text').each ( function () {
			var o = $(this) ;
			var lang = o.attr('lang') ;
			var title = o.attr('title') ;
			me.loadWikiIntro ( lang , title , function ( desc ) {
				o.html ( desc ) ;
				me.addGoogleTranslateLink ( o ) ;
			} ) ;
		} ) ;
		
		// Wikidata item view
		$('#gametile_'+tile.secnum+' div.item_preview').each ( function () {
			var o = $(this) ;
			var q = o.attr('q') ;

			// Language options
			me.wd.getItemBatch ( [q] , function () {
				var i = me.wd.getItem(q) ;
				if ( typeof i == 'undefined' ) return ;
				var links = [] ;
				links.push ( { lang:'auto' , label:'Auto' } ) ;
				var sitelinks = i.getWikiLinks() ;
				$.each ( sitelinks , function ( wiki , v ) {
					var m = wiki.match ( /^(.+)wiki$/ ) ;
					if ( m == null ) return ;
					var l = m[1] ;
					links.push ( { lang:l , label:l , page:v.title } ) ;
				} ) ;
				var h = '' ;
				$.each ( links , function ( k , v ) {
					if ( k > 0 ) h += ' | ' ;
					h += "<a href='#' class='itemoption'" ;
					h += " lang='" + v.lang + "'" ;
					h += " title='" + me.escapeAttribute(v.page) + "'" ;
					h += ">" + v.label + "</a>" ;
				} ) ;
				o.find('div.item_options').html ( h ) ;
				o.find('div.item_options a.itemoption').click ( me.clickItemOption ) ;
			} ) ;
			
			// Automatic description
			$.get ( '/autodesc/?q='+q+'&lang='+me.getMainLang()+'&mode=long&links=text&format=json&media=1&thumb=120&zoom=6' , function ( d ) {
				var desc = d.result ;
				if ( desc.match(/Cannot auto/) && d.manual_description != '' ) desc = d.manual_description ;
				
				if ( typeof d.media != 'undefined' && typeof d.media.image != 'undefined' ) {
					var image = d.media.image[0] ;
					if ( typeof d.thumbnails != 'undefined' && typeof d.thumbnails[image] != 'undefined' ) {
						var i = d.thumbnails[image] ;
						var h = "<div class='item_thumb'>" ;
						h += "<a href='" + me.escapeAttribute(i.descriptionurl) + "'>" ;
						h += "<img src='" + me.escapeAttribute(i.thumburl) + "' /></a></div>\n" ;
						desc = h + desc ;
					}
				}

				if ( typeof d.media != 'undefined' && typeof d.thumbnails.osm_map != 'undefined' ) {
					if ( typeof d.thumbnails != 'undefined' && typeof d.thumbnails.osm_map != 'undefined' ) {
						var i = d.thumbnails.osm_map ;
						var h = "<div class='item_thumb'>" ;
						h += "<a href='" + me.escapeAttribute(i.descriptionurl) + "'>" ;
						h += "<img src='" + me.escapeAttribute(i.thumburl) + "' /></a></div>\n" ;
						desc = h + desc ;
					}
				}
				
				me.autodesc_cache[q] = desc ;
				o.find('div.item_label').html ( "<b>" + me.getQlink(q,d.label) + "</b> <span class='qnum'>[" + q + "]</span>" ) ;
				o.find('div.item_description').html ( desc ) ;
				
				o.find('a').each ( function () {
					var o = $(this) ;
					o.attr ( 'target' , '_blank' ) ;
				} ) ;
				
			} , 'json' ) .fail ( function () {
				o.find('div.item_label').html ( "<b>" + me.getQlink(q) + "</b> <span class='qnum'>[" + q + "]</span>" ) ;
				o.find('div.item_description').html ( "<i>Automatic description failed.</i>" ) ;
			} ) ;
		} ) ;


		// Maps
		$.each ( maps , function ( k , v ) {
			me.renderMap ( v ) ;
		} ) ;
		
	} ,
	
	loadWikiIntro : function ( lang , title , callback ) {
		var server = lang + '.wikipedia.org' ;
		$.getJSON ( '//'+server+'/w/api.php?callback=?' , {
			action:'query',
			prop:'extracts',
			exchars:1000,
//			explaintext:1,
			titles:title ,
			format:'json'
		} , function ( d ) {
			$.each ( ((d.query||{}).pages||{}) , function ( k , v ) {
				var t = v.extract.split ( "\n" ) ;
				t[t.length-1] = t[t.length-1].replace ( /...\s*$/m , '' ) ;
				v.extract = $.trim ( t.join ( "\n" ) ) ;
				callback ( v.extract ) ;
			} ) ;
		} ) ;
	} ,
	
	clickItemOption : function () {
		var me = wikidata_distributed_game ;
		var o = $(this) ;
		var p = $(o.parents('div.item_preview').get(0)) ;
		var lang = o.attr('lang') ;
		var title = o.attr('title') ;
		var q = p.attr('q') ;
		
		
		if ( lang == 'auto' ) {
			var desc = me.autodesc_cache[q] ;
			p.find('div.item_description').html ( desc ) ;
		} else {
			me.loadWikiIntro ( lang , title , function ( desc ) {
				var o = p.find('div.item_description') ;
				o.html ( desc ) ;
				me.addGoogleTranslateLink ( o ) ;
			} ) ;
		}
		
		return false ;
	} ,


	addGoogleTranslateLink : function ( o ) {
		var me = this ;
		var text = o.text().replace(/\n/g,' ').split(/\s+/) ;
		while ( text.join(' ').length > 300 ) text.pop() ; // URL length
		text = text.join(' ') ;
		var url = 'https://translate.google.com/#auto/' + me.getMainLang() + '/' + encodeURIComponent(text) ;
		var h = '' ;
		h += "<div style='float:right;margin-left:10px;margin-bottom:5px;padding:2px;text-align:right'>" ;
		h += "<a href='" + url + "' class='external' target='_blank'>Google<br/>Translate</a>" ;
		h += "</div>" ;
		o.prepend(h);
	} ,
	
	showCurrentGame : function () {
		var me = this ;
		var g = me.current_game ;
		
		$.each ( g.cache , function ( num , tile ) {
			if ( tile.visible ) return ;
			tile.secnum = me.serial++ ;
			me.appendGame ( tile ) ;
			tile.visible = true ;
		} ) ;
		
		if ( me.vertical ) $('div.control_box').addClass ( 'control_box_vertical' ) ;
		$('#tiles div.gametile:first-child div.control_box').show() ; // Show controls for top tile
	} ,
	
	nextTile : function () {
		var me = this ;
		var g = me.current_game ;
		if ( g.cache.length > 0 ) {
			var id = g.cache[0].id ;
//			g.cache.shift() ;
			var tmp = [] ;
			$.each ( g.cache , function ( k , v ) {
				if ( v.id == id ) return ;
				tmp.push ( v ) ;
			} ) ;
			g.cache = tmp ;
		}
		$('div.gametile:first').animate({height:'toggle',opacity:0.25},500,'swing',function(){
			$('div.gametile:first').remove() ;
			me.updateCurrentGame() ;
		})
	} ,
	
	sanitizeID : function ( id ) {
		return id ; // TODO FIXME
	} ,
	
	startGame : function ( id , start_options ) {
		var me = this ;
		var g = me.games[''+id] ;
		if ( typeof g == 'undefined' ) {
			return me.showGames ( { 'class':'danger',msg:"The game you tried to play does not exists, or is deactivated. Please try another one!"} ) ;
		}
		me.current_game = g ;
		
		var h = '' ;
		h += "<div class='row lead'>" ;
		h += "<div class='col-md-12'>" ;
		if ( g.testing ) h += "<div class='alert alert-warning'>You are playing this game in test mode! <a id='store_game' href='#'>Add it permanently</a> once you've tested it.</div>" ;
		h += "<div class='game_title'>" + me.toText ( me.t ( g.label ) ) + "</div>" ;
		h += "<div class='game_description'>" + me.toText ( me.t ( g.description ) ) + "</div>" ;
		if ( typeof g.chosen_options == 'undefined' ) g.chosen_options = start_options||{} ;
		if ( typeof g.options != 'undefined' ) {
			h += "<div>" ;
			$.each ( g.options , function ( k , v ) {
				h += "<div style='display:inline-block;margin-right:30px'>" ;
				h += "<span>" + me.toText ( v.name ) + "</span> : " ;
				h += '<div class="btn-group" data-toggle="buttons">' ;
				v.input_name = 'game_options_' + me.sanitizeID ( v.key ) ;
				$.each ( v.values , function ( k2 , v2 ) {
					h += '<label class="btn btn-primary' ;
					if ( typeof g.chosen_options[v.key] == 'undefined' ) g.chosen_options[v.key] = k2 ;
					if ( g.chosen_options[v.key] == k2 ) h += ' active' ;
					h += '">' ;
					h += '<input type="radio" name="' + v.input_name + '" key="' + me.escapeAttribute(v.key) + '" value="' + me.escapeAttribute(k2) + '" autocomplete="off"' ;
					if ( g.chosen_options[v.key] == k2 ) h += ' checked' ;
					h += '> ' + v2 + '</label>' ;
				} ) ;
				h += "</div>" ;
				h += "</div>" ;
			} ) ;
			h += "</div>" ;
		}
		h += "</div></div>" ;
		h += "<div id='tiles' class='row'></div>" ;
		$('#main').html ( h ) ;
		
		$('#store_game').click ( me.storeGame ) ;
		
		$.each ( (g.options||{}) , function ( k , v ) {
			$('#main div.lead input[name="'+v.input_name+'"]').change ( function () {
				var o = $(this) ;
				var key = o.attr('key') ;
				var value = o.attr('value') ;
				g.chosen_options[key] = value ;
				g.cache = [] ;
				me.startGame ( id ) ;
			} ) ;
		} ) ;

		var loc = [] ;
		loc.push ( 'game='+id ) ;
		var j = JSON.stringify ( g.chosen_options ) ;
		if ( j != '{}' ) loc.push ( 'opt='+encodeURIComponent(j) ) ;
		if ( !g.testing ) window.location.hash = loc.join ( '&' ) ;
		
		$.each ( g.cache , function ( k , v ) { v.visible = false } ) ;
		
		me.updateCurrentGame() ;
	} ,
	
	updateCurrentGame : function () {
		var me = this ;
		var g = me.current_game ;
		if ( g.cache.length < me.precache_games ) {
			me.isRunning(1);
			var params = {
				action:'tiles' ,
				num:(me.precache_games-g.cache.length) ,
				lang:me.getMainLang()
			} ;
			$.each ( (g.chosen_options||{}) , function ( k , v ) {
				params[k] = v ;
			} ) ;
			$.getJSON ( me.getBaseAPI(g) , params , function ( d ) {
				$.each ( d , function ( dummy , tile ) {
					g.cache.push ( tile ) 
				} ) ;
			} ) .always ( function () {
				me.isRunning(-1) ;
				me.showCurrentGame() ;
			} ) ;
		}
		me.showCurrentGame() ;
	} ,
	
	getRecentChanges : function ( o , callback ) {
		var me = this ;
		var params = { action:'rc' } ;
		if ( typeof o.user != 'undefined' ) params.user = o.user ;
		if ( typeof o.max != 'undefined' ) params.max = o.max ;
		me.isRunning(1);
		$.get ( './api.php' , params , function ( d ) {
			me.isRunning(-1);
			callback ( d.data ) ;
		} ) ;
	} ,
	
	showUserSettings : function () {
		var me = wikidata_distributed_game ;
		me.current_game = undefined ;
		window.location.hash = 'mode=settings' ;
		
		var h = '' ;
		h += '<div class="panel panel-default" id="user_settings">' ;
		h += '<div class="panel-heading">Your personal settings</div>' ;
		h += '<div class="panel-body"></div>' ;
		h += '</div>' ;
		h += '<div class="panel panel-default" id="user_rc">' ;
		h += '<div class="panel-heading">Your recent edits</div>' ;
		h += '<div class="panel-body"></div>' ;
		h += '</div>' ;
		$('#main').html(h) ;
		
		function showUserSettings () {
			var h = '' ;
			h += "<form class='form form-inline'>" ;
			h += "<div>Your languages <input type='text' id='user_languages' value='" + me.user_settings.languages.join(',') + "' /> (comma-separated, primary first)</div>" ;
			h += "<div><vutton class='btn btn-primary' id='save_user_settings'>Save</button></div>" ;
			h += "</form>" ;
			$('#user_settings div.panel-body').html ( h ) ;
			$('#save_user_settings').click ( function () {
				var langs = $('#user_languages').val().replace(/\s/g,'').toLowerCase() ;
				if ( langs == '' ) langs = 'en' ;
				me.user_settings.languages = langs.split(',') ;
				me.storeUserSettings () ;
				showUserSettings() ;
			} ) ;
		}
		
		showUserSettings() ;
		

		me.getRecentChanges ( { user:me.widar.getUserName() } , function ( rc ) {
			me.renderRecentChanges ( rc , { show_user:false , target:$('#user_rc div.panel-body') } ) ;
		} ) ;
		return false ;
	} ,
	
	showStats : function () {
		var me = wikidata_distributed_game ;

		me.current_game = undefined ;
		window.location.hash = 'mode=stats' ;
		
		var h = '' ;
/*		h += '<div class="panel panel-default" id="user_settings">' ;
		h += '<div class="panel-heading">Your personal settings</div>' ;
		h += '<div class="panel-body"></div>' ;
		h += '</div>' ;*/
		h += '<div class="panel panel-default" id="recent_changes">' ;
		h += '<div class="panel-heading">Recent changes</div>' ;
		h += '<div class="panel-body" style="overflow-y:auto;height:300px"></div>' ;
		h += '</div>' ;
		$('#main').html(h) ;

		me.getRecentChanges ( {max:50} , function ( rc ) {
			me.renderRecentChanges ( rc , { show_user:true , target:$('#recent_changes div.panel-body') } ) ;
		} ) ;
		
		return false ;
	} ,
	
	renderRecentChanges : function ( rc , o ) {
		var me = this ;
		var h = "" ;
		h += "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>Game</th><th>Item(s)</th>" ;
		if ( o.show_user ) h += "<th>User</th>" ;
		h += "<th>Decision</th><th>Time <small>[UTC]</small></th></tr></thead>" ;
		h += "<tbody>" ;
		$.each ( rc , function ( k , v ) {
		
			var qs = [] ;
			var j = JSON.parse ( v.api_action ) ;
			$.each ( j , function ( k1 , v1 ) {
				if ( typeof v1.action == 'undefined' ) return ;
				var q ;
				if ( v1.action == 'wbsetsitelink' ) q = v1.id ;
				if ( v1.action == 'wbcreateclaim' ) q = v1.entity ;
				if ( v1.action == 'wbmergeitems' ) {
					qs.push ( me.getQlink(v1.fromid) ) ;
					qs.push ( me.getQlink(v1.toid) ) ;
				}
				if ( typeof q != 'undefined' ) qs.push ( me.getQlink(q) ) ;
			} ) ;
		
			j = JSON.parse ( v.json ) ;
			h += "<tr>" ;
			h += "<td><a href='#' game='"+v.game+"'>" + me.toText ( me.t ( j.label ) ) + "</a></td>" ;
			h += "<td>" + qs.join(', ') + "</td>" ;
			if ( o.show_user ) h += "<td><a href='//www.wikidata.org/wiki/User:" + me.escapeAttribute(v.user_name.replace(/ /g,'_')) + "' target='_blank'>" + v.user_name + "</a></td>" ;
			h += "<td>" + v.decision + "</td>" ;
			h += "<td>" + me.prettyTimestamp ( v.timestamp ) + "</td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		o.target.html ( h ) ;
		o.target.find('a[game]').click ( function () {
			var o = $(this) ;
			me.startGame ( o.attr('game') ) ;
		} ) ;
		if ( typeof o.callback != 'undefined' ) callback() ;
	} ,
	
	prettyTimestamp : function ( ts ) {
		return ts.substr(0,4)+'-'+ts.substr(4,2)+'-'+ts.substr(6,2)+' '+ts.substr(8,2)+':'+ts.substr(10,2)+':'+ts.substr(12,2) ;
	} ,
	
	storeUserSettings : function () {
		var me = this ;
		if ( !me.widar.isLoggedIn() ) return ;
		me.isRunning ( 1 ) ;
		$.get ( './api.php' , {
			action:'store_user_settings',
			user:me.widar.getUserName(),
			settings:JSON.stringify ( me.user_settings )
		} , function ( d ) {
		} , 'json' ) . always ( function () {
			me.isRunning ( -1 ) ;
		} ) ;
	} ,
	
	addNewGame : function () {
		var me = this ;
		me.current_game = undefined ;
		window.location.hash = 'mode=add_game' ;
		
		var url = 'https://bitbucket.org/magnusmanske/wikidata-game/src/master/public_html/distributed/?at=master' ;
		var h = '' ;
		h += "<div class='lead'>To add a new game, you will have to provide an API. Please check <a href='"+url+"' target='_blank'>the documentation</a> first.</div>" ;
		h += "<div><form id='the_form' class='form form-inline'>" ;
		h += "Your game API URL: " ;
		h += "<input type='text' style='width:500px' id='form_url' /> " ;
		h += "<input type='submit' value='Test game' />" ;
		h += "</form></div>" ;
		
		$('#main').html(h) ;
		
		$('#the_form').submit ( function () {
			var url = $('#form_url').val() ;
			me.testGame ( url ) ;
		} ) ;
		
		return false ;
	} ,
	
	storeGame : function () {
		var me = wikidata_distributed_game ;
		var g = me.current_game ;
		if ( typeof g == 'undefined' || g.id != 0 ) return false ; // Paranoia
		if ( !me.widar.isLoggedIn() ) {
			alert ( "You need to log in to store a game." ) ;
			return false ;
		}
		
		$.get ( './api.php' , {
			action:'store_game',
			user:me.widar.getUserName() ,
			api:g.api
		} , function ( d ) {
			me.games = d.data ;
			me.prepGames() ;
			me.startGame ( d.id ) ;
		} , 'json' ) ;
		
		
		return false ;
	} ,
	
	testGame : function ( url ) {
		var me = this ;
		me.current_game = undefined ;
		window.location.hash = 'mode=test_game&url=' + encodeURIComponent(url) ;
		$('#main').html ( "<i>Loading test game...</i>" ) ;
		me.games = { 0:{ api:url } } ;
		
		function failed ( msg ) {
			var h = "<div class='lead'>" ;
			h += "<div><b>" + msg + "</b></div>" ;
			h += "<div><a href='"+me.escapeAttribute(url)+"' target='_blank'>"+url+"</a></div>" ;
			h += "</div>" ;
			$('#main').html ( h ) ;
		}
		
		$.getJSON ( me.getBaseAPI(me.games[0]) , {action:'desc'} , function ( d ) {
			if ( typeof d == 'undefined' ) return failed ( 'No JSON object in your API "action=desc" response' ) ;
			if ( typeof d.label == 'undefined' ) return failed ( 'No labels in your API "action=desc" response' ) ;
			if ( typeof d.label.en == 'undefined' ) return failed ( 'No English label in your API "action=desc" response' ) ;
			if ( typeof d.description == 'undefined' ) return failed ( 'No descriptions in your API "action=desc" response' ) ;
			if ( typeof d.description.en == 'undefined' ) return failed ( 'No English description in your API "action=desc" response' ) ;
			d.api = url ;
			d.testing = true ;
			d.id = 0 ;
			me.games = { 0:d } ;
			me.prepGames() ;
			me.startGame ( 0 ) ;
		} ) .fail ( function () { failed('No response from your API, or no valid JSON!') } ) ;
	} ,
	
	
	
	fin : ''
} ;


$(document).ready ( function () {
	wikidata_distributed_game.init() ;
} ) ;