<?PHP

//ini_set('memory_limit','200M');
//set_time_limit ( 30 ) ; // Seconds

require_once ( '../php/common.php' ) ;

function setGameStatus ( $id , $status ) {
	global $db ;
	$sql = "UPDATE games SET status='" . $db->real_escape_string($status) . "' WHERE id=$id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

function updateGame ( $id ) {
	global $db ;
	$sql = "SELECT * FROM games WHERE id=$id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$o = $result->fetch_object() ;
	if ( !$o ) return ; // No such game
	
	$url = $o->api ;
	if ( preg_match('/\?/',$url) ) $url .= '&' ;
	else $url .= '?' ;
	$url .= "callback=xyz&action=desc" ;
	$r = trim ( file_get_contents ( $url ) ) ;
	if ( !preg_match ( '/^xyz\((.+)\);{0,1}\s*$/' , $r , $m ) ) return setGameStatus ( $id , 'BAD_REPLY' ) ;
	$j = json_decode ( $m[1] ) ;
	
	if ( $j == null or !is_object($j) or !isset($j->label) or !isset($j->label->en) or !isset($j->description) or !isset($j->description->en) ) return setGameStatus ( $id , 'BAD_JSON' ) ;

	$sql = "UPDATE games SET status='OK',json='" . $db->real_escape_string(json_encode($j)) . "' WHERE id=$id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

function updateGameFile () {
	global $db ;
	$fp = fopen('games.json', 'w');
	fwrite ( $fp , "{\n" ) ;
	$sql = "SELECT * FROM games ORDER BY id DESC" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$first = true ;
	while($o = $result->fetch_object()){
		if ( $o->status != 'OK' ) continue ;
		if ( $first ) $first = false ;
		else fwrite ( $fp , ",\n" ) ;
		$j = json_decode ( $o->json ) ;
		$j->id = $o->id ;
		$j->api = $o->api ;
		fwrite ( $fp , '"'.$o->id.'":'.json_encode($j) ) ;
	}
	fwrite ( $fp , "\n}" ) ;
	fclose ( $fp ) ;
}

function getUserID ( $user ) {
	global $db ;
	$u2 = $db->real_escape_string ( trim ( str_replace ( '_' , ' ' , $user ) ) ) ;
	$sql = "SELECT * FROM user WHERE name='$u2'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) return $o->id ;
	$sql = "INSERT INTO user (name,settings) VALUES ('$u2','{\"languages\":[\"en\"]}')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	return $db->insert_id ;
}

header("Connection: close");
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Content-type: application/json');

$db = openToolDB ( 'distributed_game_p' ) ; // , 'p:tools-db' ) ; # p::tools-db for persistent connection

$out = array ( 'status' => 'OK' , 'data' => array() ) ;

$action = get_request ( 'action' , '' ) ;

if ( $action == 'update_game' ) {

	$id = get_request ( 'id' , 0 ) * 1 ;
	updateGame ( $id ) ;
	updateGameFile() ;

} else if ( $action == 'update_all_games' ) {

	$ids = array() ;
	$sql = "SELECT id FROM games" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $ids[] = $o->id ;
	foreach ( $ids AS $id ) updateGame ( $id ) ;
	updateGameFile() ;

} else if ( $action == 'log_action' ) {

	$ts = date ( 'YmdHis' ) ;
	$week = preg_replace ( '/^(\d\d\d\d)-(\d)$/' , '$1-0$2' , date ( 'Y-W' ) ) ;
	
	$user = get_request ( 'user' , '' ) ;
	$game = get_request ( 'game' , 0 ) * 1 ;
	$tile = $db->real_escape_string ( get_request ( 'tile' , '' ) ) ;
	$decision = $db->real_escape_string ( get_request ( 'decision' , '' ) ) ;
	$api_action = $db->real_escape_string ( get_request ( 'api_action' , '[]' ) ) ;
	
	$uid = getUserID ( $user ) ;
	
	if ( $game != 0 ) { // Ignore game testing
		$sql = "INSERT IGNORE INTO actions (user,game,tile,timestamp,week,decision,api_action) VALUES ($uid,$game,'$tile','$ts','$week','$decision','$api_action')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	}

} else if ( $action == 'rc' ) {

	$user = get_request ( 'user' , '' ) ;
	$max = get_request ( 'max' , 20 ) * 1 ;

	$sql = "SELECT *,actions.id AS aid,user.name AS user_name FROM user,games,actions WHERE actions.game=games.id AND user.id=actions.user AND games.status='OK'" ;
	if ( $user != '' ) $sql .= " AND user.name='" . $db->real_escape_string ( $user ) . "'" ;
	$sql .= " ORDER BY timestamp DESC" ;
	$sql .= " LIMIT $max" ;
	$out['sql'] = $sql ;
	
	$out['data'] = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$out['data'][] = $o ;
	}
	
} else if ( $action == 'store_game' ) {

	$url = get_request ( 'api' , '' ) ;
	$user = get_request ( 'user' , '' ) ;
	if ( $url != '' and $user != '' ) {
		$sql = "INSERT INTO games (api,user) VALUES ('" . $db->real_escape_string ( $url ) . "','" . $db->real_escape_string ( $user ) . "')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$out['id'] = $db->insert_id ;
		updateGame ( $out['id'] ) ;
		updateGameFile() ;
		$out['data'] = json_decode ( file_get_contents ( 'games.json' ) ) ;
	}

} else if ( $action == 'get_user_settings' ) {

	$user = get_request ( 'user' , '' ) ;
	$uid = getUserID ( $user ) ;
	$sql = "SELECT * FROM user WHERE id=$uid" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()) $out['user_settings'] = json_decode ( $o->settings ) ;

} else if ( $action == 'store_user_settings' ) {

	$settings = get_request ( 'settings' , '' ) ;
	$user = get_request ( 'user' , '' ) ;
	$uid = getUserID ( $user ) ;
	$sql = "UPDATE user SET settings='" . $db->real_escape_string($settings) . "' WHERE id=$uid" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$out['status'] = 'OK' ;
	
}

print json_encode ( $out ) ;

?>
